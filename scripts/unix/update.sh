#!/bin/bash
# Pull changes from the remote repository. This is conventient to use in a "stable" git clone
# of the repository to keep it in sync with the latest updates that you merge in from your
# working repository of Voxos.

set -e

REPO_DIR=$(git rev-parse --show-toplevel) 
cd $REPO_DIR
git stash push .env
git pull
git stash pop
