#!/bin/bash

SCRIPTS_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
PROJECT_ROOT="$SCRIPTS_DIR/../.."

function check_sources() {
    local env_key_name=$1
    local key=$2
    local result=''

    if [ ! -z "$key" ]; then
        result=$key
    else
        # Search for key in environment variables
        if [ ! -z ${!env_key_name} ]; then
            result=${!env_key_name}
        fi
        # If the key is still not found load the .env file
        if [ -z "$key" ]; then
            if [ -f ./.env ]; then
                # Load the .env file if it exists
                . $PROJECT_ROOT/.env
                # Check for the key in the .env file
                if [ ! -z ${!env_key_name} ]; then
                    result=${!env_key_name}
                fi
            fi
        fi
    fi

    echo $result
}

function get_python_command(){
    VOXOS_PYTHON_VERSION="$1"
    if [ -n "$VOXOS_PYTHON_VERSION" ]; then
        if command -v py > /dev/null 2>&1; then
            py -$VOXOS_PYTHON_VERSION --version 2>&1 > /dev/null
            PYTHON_COMMAND="py -$VOXOS_PYTHON_VERSION"
        elif command -v python$VOXOS_PYTHON_VERSION > /dev/null 2>&1; then
            python$VOXOS_PYTHON_VERSION --version 2>&1 > /dev/null
            PYTHON_COMMAND="python$VOXOS_PYTHON_VERSION"
        else
            echo ""
        fi
    else
        PYTHON_VERSION=$(python3 --version | cut -d' ' -f2)
        PYTHON_COMMAND="python3"
    fi
    echo $PYTHON_COMMAND
}

function find_virtual_env(){
    VENV_PATHS=("Scripts" "bin")
    for venv_path in "${VENV_PATHS[@]}"; do
        if [ -d "$SCRIPTS_DIR/../../.venv/$venv_path" ]; then
            VENV_DIR=.venv/$venv_path
            break 2
        fi
    done
    echo $VENV_DIR
}
