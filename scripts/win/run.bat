@echo off
setlocal enabledelayedexpansion

set SCRIPTS_DIR=%~dp0
set PROJECT_ROOT=%SCRIPTS_DIR%..\\..\\

rem -- Check for the virtual environment directory
if exist "%PROJECT_ROOT%.venv\\Scripts\\" (
    set VENV_DIR="%PROJECT_ROOT%.venv\\Scripts\\"
) else if exist "%PROJECT_ROOT%.venv\\bin\\" (
    set VENV_DIR="%PROJECT_ROOT%.venv\\Scripts\\"
) else (
    echo No virtual environment found
    exit /b 1
)

call "%VENV_DIR%\\activate"

set verbose=0
set openai_api_key=
set recording_hotkey=
set mute_system_volume=

:: Parse command line arguments
:parse_args
if "%~1"=="" goto :end_parse_args
if "%~1"=="-v" (
    set "verbose=1"
) else if "%~1"=="-o" (
    set "openai_api_key=%~2"
    shift
) else if "%~1"=="-r" (
    set "recording_hotkey=%~2"
    shift
) else if "%~1"=="-m" (
    set "mute_system_volume=%~2"
    shift
) else if "%~1"=="-e" (
    set "editor=%~2"
    shift
) else (
    echo "Usage: %~0 [-v] [-o openai_api_key] [-r recording_hotkey] [-m mute_bool] [-e editor]" >&2
    exit /b 1
)
shift
goto :parse_args
:end_parse_args

if "%verbose%"=="1" (
    set VOXOS_VERBOSE_ARGS=--verbose
)

call %SCRIPTS_DIR%check_sources.bat VOXOS_OPENAI_API_KEY PROVIDED_KEY %openai_api_key%
if "%PROVIDED_KEY%"=="" (
    echo OpenAI API key not found. Please set it in the environment variable VOXOS_OPENAI_API_KEY, pass it as an argument to this script, or define it in a .env file
    exit /b 1
)

echo Checking OpenAI API key validity...
set "response="
for /f "delims=" %%i in ('curl -s "https://api.openai.com/v1/models" -H "accept: application/json" -H "Authorization: Bearer %PROVIDED_KEY%"') do set "response=!response! %%i"
echo !response! | findstr /C:"error" >nul 2>&1
if %errorlevel%==0 (
    echo    The provided OpenAI API key is invalid. You must either set it when calling this script, set it in the environment variable VOXOS_OPENAI_API_KEY, or define it in a .env file    
    exit /b 1
)

call %SCRIPTS_DIR%check_sources.bat VOXOS_RECORDING_HOTKEY RECORDING_HOTKEY %recording_hotkey%
if not "%RECORDING_HOTKEY%"=="" (
    set RECORDING_HOTKEY_ARGS=--recording_hotkey %RECORDING_HOTKEY%
)

call %SCRIPTS_DIR%check_sources.bat VOXOS_MUTE_SYSTEM_VOLUME MUTE_SYSTEM_VOLUME %mute_system_volume%
if not "%MUTE_SYSTEM_VOLUME%"=="" (
    if "%MUTE_SYSTEM_VOLUME%"=="0" (
        set MUTE_SYSTEM_VOLUME_ARGS=--no_mute
    )
)

call %SCRIPTS_DIR%check_sources.bat VOXOS_EDITOR EDITOR %editor%
if not "%EDITOR%"=="" (
    set EDITOR_ARGS=--editor %EDITOR%
)

call %SCRIPTS_DIR%check_sources.bat VOXOS_VERBOSE VERBOSE
if not "%VERBOSE%"=="" (
        echo Setting verbseness to %VERBOSE%...
        set VOXOS_VERBOSE_ARGS=--verbose
)

call %SCRIPTS_DIR%check_sources.bat VOXOS_LOG_LEVEL LOG_LEVEL
if not "%LOG_LEVEL%"=="" (
        echo Setting log level to %LOG_LEVEL%...
        set VOXOS_LOG_LEVEL_ARGS=--log_level %LOG_LEVEL%
)

echo Sourcing virtual environment...
call "%VENV_DIR%\activate"
cd "%PROJECT_ROOT%"
for /f "tokens=2 delims= " %%a in ('python -V') do (
    set version=%%a
)
set major_version=%version:~0,1%
set minor_version=%version:~2,2%

if not "%VOXOS_VERBOSE%"=="" (
    if "%VOXOS_VERBOSE%"=="1" (
        echo Running in verbose mode...
        set VOXOS_VERBOSE_ARGS=--verbose
    )
)

if "%major_version%"=="3" (
    set /a minor_version_int=%minor_version%
    set /a lower_limit=8
    set /a upper_limit=12
    if !minor_version_int! geq !lower_limit! (
        if !minor_version_int! leq !upper_limit! (
            python -m main --openai_api_key %PROVIDED_KEY% %RECORDING_HOTKEY_ARGS% %VOXOS_VERBOSE_ARGS% %VOXOS_LOG_LEVEL_ARGS% %MUTE_SYSTEM_VOLUME_ARGS% %EDITOR_ARGS%
        )
    )
)

if not "%major_version%"=="3" (
    echo Python version in the activated venv is not supported. Versions 3.8 to 3.12 are supported.
)

