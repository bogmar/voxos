@echo off
setlocal enabledelayedexpansion

REM install.bat - Installs Voxos in a Windows environment

REM The script takes the following actions:

REM 1. It attempts to detect the Python version installed on the system. 
REM    - If a specific Python version is mentioned in the .env file using the VOXOS_PYTHON_VERSION variable, the script will use that version. 
REM      If that version is not installed on the system, the script will output an error message and stop execution.
REM    - If no version is indicated in .env, it assumes the system default Python3 version.

REM 2. The script verifies that the Python version is in the range of 3.8 to 3.12. If it is out of this range, 
REM    an error message is printed and the script stops execution.

REM 3. It checks for the existence of a hidden virtual environment named .venv or venv, preferring the first if both exist. If no virtual 
REM    environment is detected, one is created using the version of Python that was determined in the previous steps. 

REM 4. The virtual environment is activated and dependencies are installed from a requirements.txt file located 
REM    in the parent directory of the .venv or venv.

REM Please note that this script needs to be run from parent directory of the virtual environment and
REM it assumes a directory structure where the .env file and the requirements.txt file are in parent directory of 
REM venv or .venv folder, and the script itself is in a nested directory. 

set SCRIPTS_DIR=%~dp0

echo Checking Python version...
@echo off
for /f "delims=# tokens=1*" %%a in ('type %SCRIPTS_DIR%..\\..\\.env') do (
    set "line=%%a"
    for /f "tokens=1,2 delims==" %%g in ("!line!") do (
        set "%%g=%%h"
    )
)
set VOXOS_PYTHON_VERSION=!VOXOS_PYTHON_VERSION: =!
if defined VOXOS_PYTHON_VERSION (
    py -%VOXOS_PYTHON_VERSION% --version >nul 2>&1
    if errorlevel 1 (
        echo Python version %VOXOS_PYTHON_VERSION% mentioned in .env is not installed on the system.
        exit /b 1
    ) else (
        set PYTHON_VERSION=%VOXOS_PYTHON_VERSION%
    )
) else (
    echo No Python version provided in .env, using system default Python version.
    for /f "tokens=2" %%a in ('python --version') do set PYTHON_VERSION=%%a
)
for /f "tokens=1,2 delims=." %%a in ("%PYTHON_VERSION%") do (
    set PYTHON_MAJOR=%%a
    set PYTHON_MINOR=%%b
)
if %PYTHON_MAJOR% lss 3 (
    echo Python 3.8 or newer is required.
    exit /b 1
) else if %PYTHON_MAJOR% gtr 3 (
    echo Python 3.12 or older is required.
    exit /b 1
) else if %PYTHON_MAJOR% equ 3 (
    if %PYTHON_MINOR% lss 8 (
        echo Python 3.8 or newer is required.
        exit /b 1
    ) else if %PYTHON_MINOR% gtr 12 (
        echo Python 3.12 or older is required.
        exit /b 1
    )
)
echo Using Python version %PYTHON_VERSION%, if you'd like to use a different version that's already installed, indicate that version in .env using VOXOS_PYTHON_VERSION.
timeout /t 1 /nobreak >nul

if exist %SCRIPTS_DIR%..\..\.venv (
    echo Using virtual environment at %SCRIPTS_DIR%..\..\.venv
) else if exist %SCRIPTS_DIR%..\..\venv (
    echo Using virtual environment at %SCRIPTS_DIR%..\..\venv
) else (
    echo No virtual environment found, creating one...
    py -%PYTHON_VERSION% -m venv %SCRIPTS_DIR%..\..\.venv
    if not exist %SCRIPTS_DIR%..\..\.venv (
        echo No virtual environment found
        exit /b 1
    )
)

timeout /t 3 /nobreak >nul

call %SCRIPTS_DIR%..\..\.venv\Scripts\activate

pip install -r %SCRIPTS_DIR%..\\..\\requirements.txt
