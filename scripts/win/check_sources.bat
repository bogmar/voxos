@echo off

set SCRIPTS_DIR=%~dp0
set PROJECT_ROOT=%SCRIPTS_DIR%..\\..\\

set env_var_name=%~1
set output_var_name=%~2
set command_line_var=%~3
set result=

if not "%command_line_var%"=="" (
    set result=%command_line_var%
) else (
    REM Search for key in environment variables
    for /F "tokens=1,2 delims==" %%a in ('set') do (
        if "%%a"=="!env_key_name!" (
            set result=%%b
        )
    )
    REM If the key is still not found load the .env file
    if "%result%"=="" (
        if exist "%PROJECT_ROOT%\.env" (
            for /f "delims=# tokens=1*" %%a in ('type %SCRIPTS_DIR%..\\..\\.env') do (
                set "line=%%a"
                for /f "tokens=1,2 delims==" %%g in ("!line!") do (
                    if "%%g"=="!env_var_name!" (
                        set result=%%h
                        set result=!result: =!
                        goto :end
                    )
                )
            )
        )
    )
)
:end
set %output_var_name%=%result%

endlocal
