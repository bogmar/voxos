# Contributing to Voxos

🌟 First off, thanks for taking the time to contribute! 🌟

We are really glad you're reading this, because we're always looking for volunteers to help make Voxos even better. Here are some guidelines that will help you get started.

## Code of Conduct

Our community is dedicated to providing a welcoming and inclusive experience for everyone. We expect everyone who participates in Voxos to abide by our [Code of Conduct](CODE_OF_CONDUCT.md). Please read it before participating.

## How Can I Contribute?

Contributions to Voxos are not just limited to coding. Here are different ways you can help out:

- Being a user!
- Reporting bugs.
- Suggesting enhancements.
- Writing or improving documentation.
- Submitting merge requests with bug fixes or new features.

## Word To The Wise

It is beneficial to keep two clones of the repo on your local machine. One is your working clone, where you add changes, and the other is your stable clone, which runs while you're working. Running the update script allows you to swiftly update the stable copy and then relaunch it with the latest `dev` changes, while you continue to add new features with your working clone.

## Reporting Bugs

Bugs are tracked as GitLab issues. Before creating an issue, please perform a search to see if the problem has already been reported. If it hasn't, feel free to open a new issue!

In your issue, please include:

- A clear title.
- A detailed description.
- OS information.
- Steps to reproduce the issue.
- Expected behavior and what you see instead.
- Any relevant screenshots or logs.

## Suggesting Enhancements

Enhancement suggestions are also tracked as GitLab issues. Like bug reports, please perform a search before opening a new issue to see if the enhancement has already been suggested. For new enhancement suggestions, provide a clear and detailed explanation of the feature you have in mind.

## Fork & Pull Request Strategy

External contributors are encouraged to use a fork and pull request strategy:

1. **Fork the repository** on GitLab.
2. **Create a new branch** in your fork for your modifications.
3. **Commit your changes** and push them to your fork.
    1. Organize your commits into logical units - using rebase, squash and edit to the community's advantage.
    2. Rebase against the latest `dev` branch before opening your merge request.
4. **Open a Merge Request** on the original repository.
   - Target the `dev` branch for your merge requests.
   - Provide a clear and detailed title and description of your changes.
   - Link any relevant issues.

## Merge Request (MR) Acceptance Criteria

Your MR must adhere to the following criteria before it can be accepted:
- It targets the `dev` branch.
- It contains both a clear, short title and detailed description providing motivation and summaries for the changes. Try to answer "why" the changes are necessary - the "what" will be clear from the diff.
    - Having difficutly fitting everything into a short title? You should probably consider breaking it up into multiple MRs.
- Its commits are organized into logical units.
    - Consider rebasing locally to combine related commits and re-order the remaining ones to tell a logical story. This can be easily done with `git reset --mixed <commit>` where commit is the commit hash of the commit before your first commit. After that, add your changes in logical batches to create each new commit until all changes are committed.
- Its new modules contain the appropriate license header (GNU GPLv3) with the current year and your name/organization. 
- It does not modify the license header of existing files.

It is mightly recommended that your MR also adheres to these additional criteria:
- It has been rebased against the latest `dev` branch.

## Coding Standards

Please ensure your code adheres to the project's existing design patterns and coding standards. If you're unsure, please ask!

## Testing

We encourage you to write tests for any new code or changes you make to ensure the quality and functionality of Voxos.

## Questions?

Feel free to ask any questions you may have on the process of contributing. Our community is here to help!
