# CHANGELOG.md

## [0.7.2] - 2024-01-19

### Fixed
- Fixes incorrect path reference created by find_virtual_env util.
- Fixes duplicate call to chmod in CI pipeline.

## [0.7.1] - 2024-01-19

### Fixed
- Resolved character encoding issue for non-Latin characters.
- Install file now calls pip with the found version of Python.
- Installs missing portaudio in CI test stage allowing install.sh to complete successfully.

## [0.7.0] - 2024-01-19

### Added
- Supports retaining memory when servicing requests and sets this as the default.
- Supports toggling the agent memory on and off from the context menu.
- Adds tooltips to all context menu items.

### Changed
- Re-enables clearing clipboard once the runner completes. Otherwise the clipboard content is always included in a subsequent run which is not desirable especially with memory enabled.
- Caputures clipboard content when recording starts instead of when the runner is invoked.
- Combines configuration actions into a "Settings" submenu in the context menu.

## [0.6.1] - 2024-01-19

### Added
- Supports specifying the VOXOS_EDITOR environment variable to specify the preferred text editor.
- Added .gitlab-ci.yml for CI/CD, with version checking, and test stage placeholder.

## [0.6.0] - 2024-01-19

### Added
- Implemented naive and truncating summarization types.
- Added additional requirements.txt on tiktoken.
- Improved runner self-naming, now the runner takes both spoken input and clipboard into consideration when shows its name in the Runners context menu.

### Changed
- Removed any self_hosted arguments and logic as they are unecessary.
- Fixed run.sh reference to python, now uses py instead.
- Fixed bad f-strings in openai API call responses on error.

## [0.5.0] - 2024-01-15

### Added
- Mutex on the audio recorder to prevent segfaults.
- Windows system volume controller to prevent segfaults.
- Improved error handling on failed OpenAI API responses.

### Changed
- Fixed failing check on the validity of the provided OpenAI API key.

## [0.4.0] - 2024-01-15

### Added
- Added common context menu, allows for force stopping runners.
- Refactored common context menu out of HUD and tray icon, added Runners context menu and runner sub action items to kill a specific runner, used gpt-3.5-turbo to create a runner name label.

### Changed
- Fixed context menu signals.
- Clicking on HUD now toggles recording.
- Moved the processing indicator to the new bubble widget that now shows the number of active runners.

## [0.3.0] - 2024-01-14

### Added
- Added `.env` file placeholder for specifying various preferences.
- Added scripts for Windows: `install.bat`, `run.bat`, `update.bat`.
- Added checks on validity of the provided OpenAI API key.
- Added Usage Guide and reference to README.md.
- Added HUD widget for showing the current recording status.
- Added Cursor utility to handle interactions related to the cursor.
- Added configuration files for VSC settings.

## [0.2.0] - 2023-12-26

### Added
- Added TextEditor class for supporting text editing interaction.
- Added AgentService for access to LLM completions agents - OpenAI GPT Complections currently supported.

### Changed
- Refactored core modules.

## [0.1.0] - 2023-12-21

### Added
- Added for modules and transcription functionality.

