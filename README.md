# Voxos [Beta Release] - Voice Your Desires🎙️🔴✨

Voxos is a versatile and user-friendly voice assistant for your desktop computer. It streamlines integration of LLMs into your day-to-day workflows compared using web UIs to access those LLMs. It is ideal for anyone that uses a desktop computer and wants to save time and effort.

You can also build on Voxos' modular design to add your own custom functionality. Voxos is designed to be easy to extend and customize. With that said, you are encouraged to tailor your modifications in a way that adhere to the current design patterns in the hope that you will contribute by opening an MR that benefits all users of Voxos.

## Features 🌟

- Compact HUD and System Tray Icon interfaces.
- Parallel processing of voice commands.
- Configurable agent memory settings.
- Real-time voice command transcription with OpenAI's whisper-1.
- Supports:
    - OpenAI's chat completions (ex. gpt-3.5-turbo, gpt-4, gpt-4-1106-preview)
    - ... BYOLLM coming soon ...

## Getting Started 🚀

### Prerequisites

- Installation of Python 3.8 or higher
- Access to an OpenAI API key

### Configuration

The bare minimum configuration required is that your OpenAI API key is available as the VOXOS_OPENAI_API_KEY variable in either the environment or in the [.env](./.env) file placehold included the root directory of the project.

### Installation

Clone the repository:

```bash
git clone git@gitlab.com:literally-useful/voxos.git
```
Install the required packages:

```bash
# Unix or WSL environments
./scripts/unix/install.sh
```
```batch
:: Windows
scripts\win\install.bat
```

## Basic CLI Usage Instructions 📖

First, start Voxos by running `./scripts/unix/run.sh` in a Unix environment or `scripts\win\run.bat` on Windows.

### Talk to Voxos About What's in Your Clipboard

1. Copy any text of interest to the clipboard.

2. Start Recording: Click once on the HUD or use the recording hotkey. (default: ctrl+alt+z)
    - Voxos will use your clipboard content available at the moment the recording started as additional input.

3. Talk to Voxos: Tell Voxos what to do with the contents in the clipboard, or ask a follow-up question to the response Voxos gives you.

4. Stop Recording: Click once on the HUD or use the recording hotkey.
    - Voxos will clear your clipboard content once the recording stops to prepare for your next command.

5. After a short moment, Voxos will open up your system note pad with its response.

You don't have to wait for Voxos to finish processing your request before you start recording again. You can record as many requests as you want in a row and manage them from the HUD's right-click context menu.

## Advanced Usage Instructions 🔮

For a full list of features, please refer to the [User Guide](docs/user_guide.md).

## Contributing 🤝

We love your input! We want to make contributing to Voxos as easy and transparent as possible, whether it's:

- Reporting a bug 🐛
- Discussing the current state of the code 📊
- Submitting a fix 🔨
- Proposing new features 💡
- Becoming a maintainer 🛠️

We use GitLab to host code, track issues, and feature requests, as well as accept pull requests. Please see [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## Feedback and Support ❤️

If you have any feedback or need support, please open an issue.

## License 📄

Voxos is released under the GNU GPLv3.0 License. See [COPYING](COPYING) for a copy of the full license.


<br><br>

Copyright (c) 2023 Literally Useful LLC

https://www.literallyuseful.com/

